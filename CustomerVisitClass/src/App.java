import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("Nam");
        Customer customer2 = new Customer("Lan");
        customer2.setMember(true);
        customer2.setMemberType("Gold");
        System.out.println("Customer1 la: " + customer1);
        System.out.println("Customer2 la: " + customer2);
        Visit visit1 = new Visit(customer1, new Date(2022, 7, 3));
        Visit visit2 = new Visit(customer2, new Date());
        visit1.setServiceExpense(1.99);
        visit1.setProductExpense(1.59);
        visit2.setServiceExpense(3.49);
        visit2.setProductExpense(2.29);
        System.out.println("Visit1 la: " + visit1);
        System.out.println("Visit2 la: " + visit2);
        System.out.println("Tổng chi phí visit1 là: " + visit1.getTotalExpense());
        System.out.println("Tổng chi phí visit2 là: " + visit2.getTotalExpense());
    }
}
