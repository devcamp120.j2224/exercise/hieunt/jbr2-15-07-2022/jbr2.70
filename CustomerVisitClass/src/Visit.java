import java.util.Date;

public class Visit {
    Customer customer;
    Date date;
    double serviceExpense;
    double productExpense;
    public Visit(Customer customer
        , Date date) {
        this.customer = customer;
        this.date = date;
    }
    public String getName() {
        return customer.getName();
    }
    public double getServiceExpense() {
        return serviceExpense;
    }
    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }
    public double getProductExpense() {
        return productExpense;
    }
    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }
    public double getTotalExpense() {
        return serviceExpense + productExpense;
    }
    @Override
    public String toString() {
        return "Visit[" + customer
        + ", date=" + date
        + ", productExpense=" + productExpense
        + ", serviceExpense=" + serviceExpense + "]";
    }
}
